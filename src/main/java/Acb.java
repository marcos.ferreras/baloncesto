
import dto.Jugador;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class Acb extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException {
        if(req.getParameter("RESET") != null){
            resetVotes(res);
        } else if (req.getParameter("VOTE") != null) {
            votePlayer(req, res);
        } else if (req.getParameter("VER_VOTOS") != null) {
            viewVotes(req,res);
        }
    }

    public void resetVotes(HttpServletResponse res) throws IOException {
        // Llamada a la página de confirmacion de borrado
        bd.eliminarVotos();
        res.sendRedirect(res.encodeURL("aviso-reset.html"));
    }

    private void votePlayer (HttpServletRequest req, HttpServletResponse res) throws IOException{
        HttpSession s = req.getSession(true);
        String nombreP = req.getParameter("txtNombre");
        String nombre = req.getParameter("R1");
        if (nombre.equals("Otros")) {
            nombre = req.getParameter("txtOtros");
        }
        if (bd.existeJugador(nombre)) {
            bd.actualizarJugador(nombre);
        } else {
            bd.insertarJugador(nombre);
        }
        s.setAttribute("nombreCliente", nombreP);
        // Llamada a la página jsp que nos da las gracias
        res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
    }

    public void viewVotes(HttpServletRequest req, HttpServletResponse res) throws IOException {
        HttpSession s = req.getSession(true);
        ArrayList<Jugador> jugadores = bd.obtenerJugadores();
        s.setAttribute("jugadores", jugadores);
        res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
    }
    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
