import dto.Jugador;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;

public class ModeloDatos {

    private Connection con;
    private Statement set;
    private PreparedStatement stmt;
    private ResultSet rs;
    Logger logger = LoggerFactory.getLogger(ModeloDatos.class);

    public ModeloDatos(DataSource dataSource) throws SQLException {
        con = dataSource.getConnection();
    }
    public ModeloDatos()  {
    }

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            logger.error("No se ha podido conectar");
            logger.error(String.format("El error es: %s", e.getMessage()));
        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            logger.error("No lee de la tabla");
            logger.error(String.format("El error es: %s", e.getMessage()));
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            logger.error("No modifica la tabla");
            logger.error(String.format("El error es: %s", e.getMessage()));
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            logger.error("No inserta en la tabla");
            logger.error(String.format("El error es: %s", e.getMessage()));
        }
    }

    public int obtenerVotosJugador(String nombre) {
        int votos = 0;
        try {
            stmt = con.prepareStatement("SELECT votos FROM Jugadores WHERE nombre = ?");
            stmt.setString(1, nombre);
            rs = stmt.executeQuery();

            while (rs.next()) {
                votos = rs.getInt("votos");
            }
            rs.close();
        } catch (Exception e) {
            // No lee de la tabla
            logger.error("No lee de la tabla");
            logger.error(String.format("El error es: %s", e.getMessage()));
        }
        return votos;
    }

    public ArrayList<Jugador> obtenerJugadores(){
        ArrayList<Jugador> jugadores = new ArrayList<>();
        try {
            stmt = con.prepareStatement("SELECT * FROM Jugadores");
            rs = stmt.executeQuery();

            while (rs.next()) {
                Jugador jugador = new Jugador();
                jugador.setVotes(rs.getInt("votos"));
                jugador.setPlayerName(rs.getString("nombre"));
                jugador.setId(rs.getInt("id"));
                jugadores.add(jugador);
            }
            rs.close();
        } catch (Exception e) {
            // No lee de la tabla
            logger.error("No lee de la tabla");
            logger.error(String.format("El error es: %s", e.getMessage()));
        }
        return jugadores;
    }

    public void eliminarVotos(){
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=0");
            rs.close();
            set.close();
        } catch (Exception e) {
            logger.error("No se eliminan los votos");
            logger.error(String.format("El error es: %s", e.getMessage()));
        }
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

}
