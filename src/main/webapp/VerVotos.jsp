<%@ page import="java.util.ArrayList" %>
<%@ page import="dto.Jugador" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
    <title>Recuento de votos</title>
</head>
<body>
<h1>
    Votos registrados
</h1>
<table class="votes-table">
    <tr>
        <th>ID</th>
        <th>Jugador</th>
        <th>Votos</th>
    </tr>
    <c:forEach items="${jugadores}" var="jugador">
        <tr class="table-row">
            <td>${jugador.id}</td>
            <td class="row-player">${jugador.playerName}</td>
            <td class="row-votes">${jugador.votes}</td>
        </tr>
    </c:forEach>
</table>
<div>
    <a href="index.html"> Ir al comienzo</a>
</div>
</body>
</html>
