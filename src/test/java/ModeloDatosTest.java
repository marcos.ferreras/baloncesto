import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import javax.sql.DataSource;
import static org.junit.jupiter.api.Assertions.*;
import java.sql.SQLException;

public class ModeloDatosTest {

    private static final String JDBC_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;MODE=MySQL";
    private static final String USER = "junit";
    private static final String PASSWORD = "junit";
    private static final String SCRIPT_PATH = "src/test/db/schema.sql";

    @BeforeAll
    public static void createSchema() throws Exception {
        RunScript.execute(JDBC_URL, USER, PASSWORD, SCRIPT_PATH, null, false);
    }

    private DataSource getDataSource() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL(JDBC_URL);
        dataSource.setUser(USER);
        dataSource.setPassword(PASSWORD);
        return dataSource;
    }
    @Test
    public void testExisteJugador() throws SQLException {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos(getDataSource());
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        //fail("Fallo forzado.");
    }
    @Test
    public void testVotarJugador() throws SQLException {
        System.out.println("Prueba de testVotarJugador");
        String playerName = "Llull";
        ModeloDatos instance = new ModeloDatos(getDataSource());
        int votosPrevios = instance.obtenerVotosJugador(playerName);
        instance.actualizarJugador(playerName);
        int votosPosteriores = instance.obtenerVotosJugador(playerName);
        assertEquals(votosPrevios + 1, votosPosteriores);
    }
}
