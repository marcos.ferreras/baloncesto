import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PruebasPhantomjsIT {
    private static WebDriver driver = null;
    private static final String serverBaseUrl="http://localhost:8080";

    @BeforeAll
    public static void setUp(){
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new
                String[]{"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
    }

    @Test
    public void tituloIndexTest() {
        driver.navigate().to(serverBaseUrl+"/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(),
                "El titulo no es correcto");
        System.out.println(driver.getTitle());
    }

    @Test
    public void resetVotosTest(){
        driver.navigate().to(serverBaseUrl+"/Baloncesto/");
        WebElement resetButton = driver.findElement(By.name("RESET"));
        resetButton.click();
        assertEquals(driver.getCurrentUrl(), serverBaseUrl+"/Baloncesto/aviso-reset.html");
        driver.navigate().to(serverBaseUrl+"/Baloncesto/");
        WebElement viewVotesButton = driver.findElement(By.name("VER_VOTOS"));
        viewVotesButton.click();
        List<WebElement> playersVotes = driver.findElements(By.className("row-votes"));
        for (WebElement votes: playersVotes) {
            assertEquals("0", votes.getText());
        }
    }

    @Test
    public void addVoteToOtherTest(){
        String playerToVote="CEISS Player";
        driver.navigate().to(serverBaseUrl+"/Baloncesto/");
        WebElement radioOtherPlayer = driver.findElement(By.id("item-other"));
        radioOtherPlayer.click();
        WebElement textOtherPlayer = driver.findElement(By.name("txtOtros"));
        textOtherPlayer.sendKeys(playerToVote);
        WebElement voteButton = driver.findElement(By.name("VOTE"));
        voteButton.click();
        assertTrue(driver.getCurrentUrl().matches( serverBaseUrl+"/Baloncesto/TablaVotos\\.jsp.*"));
        driver.navigate().to(serverBaseUrl+"/Baloncesto/");
        WebElement viewVotesButton = driver.findElement(By.name("VER_VOTOS"));
        viewVotesButton.click();
        List<WebElement> tableRows = driver.findElements(By.cssSelector("tr.table-row"));
        boolean playerFound = false;
        for (WebElement row: tableRows) {
            String player = row.findElement(By.className("row-player")).getText();
            if(player.equals(playerToVote)){
                String votes = row.findElement(By.className("row-votes")).getText();
                assertEquals("1", votes);
                playerFound = true;
            }
        }
        assertTrue(playerFound);
    }

    @AfterAll
    public static void cleanUp(){
        driver.close();
        driver.quit();
    }
}